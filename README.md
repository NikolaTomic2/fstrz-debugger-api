To use this server you must have *yarn* installed

First you must execute:
```sh
yarn install
```

Then you can start the server with:
```sh
yarn start
```

To specify the port you want use:
```sh
PORT=8080 yarn start
```

To launch the tests:
```sh
yarn test
```

To build the server to the build/ folder:
```sh
yarn build
```
