import { expect } from 'chai';
import is_url from './is_url';

describe('is_url()', () => {
    it('should reject an empty string', function () {
        expect(is_url("")).to.equal(false);
    })
    it('should reject a non url string', function () {
        expect(is_url("hello")).to.equal(false);
        expect(is_url("http//a.valid.url")).to.equal(false);
        expect(is_url("jfieowpqj")).to.equal(false);
        expect(is_url("\x10")).to.equal(false);
    })
    it('should reject an non http url', function () {
        expect(is_url("bolt://example.com")).to.equal(false);
        expect(is_url("ftp://example.com")).to.equal(false);
        expect(is_url("htt://example.com")).to.equal(false);
    })
    it('should accept an http url', function () {
        expect(is_url("http://example.com")).to.equal(true);
        expect(is_url("http://yay.com")).to.equal(true);
        expect(is_url("https://example.com")).to.equal(true);
    })
})