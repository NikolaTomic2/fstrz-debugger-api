import { expect } from 'chai';
import sinon from 'sinon';
import axios from 'axios';
import get_debug_data from './get_debug_data';

describe('get_debug_data()', () => {
    it('should return the status code of the response', async function () {
        sinon.stub(axios, 'get').resolves(Promise.resolve({status: 200}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('statusCode').equal(200);
        sinon.restore();

        sinon.stub(axios, 'get').resolves(Promise.resolve({status: 204}));
        const result2 = await get_debug_data('http://example.com');
        expect(result2).to.have.property('statusCode').equal(204);
    })

    it('should return the status code of the response', async function () {
        sinon.stub(axios, 'get').throwsException();
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('error');
    })

    it('should set plugged to true if the server is fasterize', async function () {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: { 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('plugged').equal(true);
    })

    it('should set plugged to false if the server isn\'t fasterize', async function () {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: { 'server': 'test'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('plugged').equal(false);
    })

    it('should set plugged to false if the server isn\'t specified', async function () {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('plugged').equal(false);
    })

    it('should set cloudfrontPOP to the specified location', async () => {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {'x-amz-cf-pop': 'OSL2-CL1', 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('cloudfrontPOP').equal('Oslo');
    })

    it('should set cloudfrontPOP to an empty string when invalid', async () => {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {'x-amz-cf-pop': 'AAA2-CL1', 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('cloudfrontPOP').equal('');
    })

    it('should set cloudfrontStatus to HIT', async () => {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {'x-cache': 'Hit from cloudfront', 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('cloudfrontStatus').equal('HIT');
    })

    it('should set cloudfrontStatus to MISS', async () => {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {'x-cache': 'Miss from cloudfront', 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('cloudfrontStatus').equal('MISS');
    })

    it('should set fstrzFlags to the appropriate values', async () => {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {'x-fstrz': 'o,c', 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('fstrzFlags').contain('cachée');
    })

    it('should set fstrzFlags to the appropriate values 2', async () => {
        sinon.stub(axios, 'get').resolves(Promise.resolve({headers: {'x-fstrz': 'w,p,o,c', 'server': 'fasterize'}}));
        const result = await get_debug_data('http://example.com');
        expect(result).to.have.property('fstrzFlags').contain('optimisation en cours');
    })

    afterEach(() => {
        sinon.restore();
    })
})