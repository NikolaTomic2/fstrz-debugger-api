import axios from 'axios';
import cloudfront_locations from '../cloudfront-edge-locations.json';
import fstrz_flags from './fstrz_flags';

export type DebugData = {
    statusCode: number,
    plugged: boolean,
    fstrzFlags?: string[],
    cloudfrontStatus?: string,
    cloudfrontPOP?: string
};

export type DebugError = {
    error: string
};

function get_cloudfront_status(x_cache: string): string {
    switch (x_cache) {
        case 'Hit from cloudfront': return 'HIT';
        case 'Miss from cloudfront': return 'MISS';
        default: return '';
    }
}

function get_cloudfront_location(cloudfront_code: string): string {
    if (!cloudfront_code) return '';
    const location_code = cloudfront_code.slice(0, 3);
    const location = (cloudfront_locations['nodes'] as any)[location_code];
    return location && location['city'] ? location['city'] : '';
}

function get_fstrz_flags(flags: string): string[] {
    if (!flags) return [];
    const p = (key: string | undefined, keys: string[], flags: string, acc: string[]): string[] => {
        if (!key) return acc;
        const regex = new RegExp(key);
        const next_key = keys.shift();
        if (!regex.test(flags))
            return p(next_key, keys, flags, acc);
        const flag = (fstrz_flags as any)[key];
        return p(next_key, keys, flags.replace(regex, ''), acc.concat(flag));
    };

    const keys = Object.keys(fstrz_flags);
    return p(keys.shift(), keys, flags, []);
}

function generate_debug_data(status: number, headers: any): DebugData {
    let response: DebugData = {statusCode: status, plugged: false};
    response.plugged = headers && headers['server'] === 'fasterize';
    if (!response.plugged) return response;
    response.cloudfrontPOP = get_cloudfront_location(headers['x-amz-cf-pop']);
    response.cloudfrontStatus = get_cloudfront_status(headers['x-cache']);
    response.fstrzFlags = get_fstrz_flags(headers['x-fstrz']);
    return response;
}

export default async function get_debug_data(url: string): Promise<DebugData | DebugError> {
    try {
        const res = await axios.get(url);
        return generate_debug_data(res.status, res.headers);
    } catch (error) {
        if (error.response) {
            const res = error.response;
            return generate_debug_data(res.status, res.headers);
        }
        return {
            error: error.code
        };
    }
}
