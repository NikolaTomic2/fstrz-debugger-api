import { Request, Response } from 'express';
import is_url from '../helpers/is_url';
import get_debug_data, {DebugData, DebugError} from '../helpers/get_debug_data';

type DebugQueryParams = {
    url: string;
};

type DebugRequest = Request<{}, {}, {}, DebugQueryParams>;

class DebugController {
    static async debug_url(req: DebugRequest, res: Response): Promise<Response> {
        const url: string = req.query['url'];

        if (!is_url(url))
            return res.status(406).send({ error: 'not a valid http url' });

        const debug_data = await get_debug_data(url);
        if ((debug_data as DebugError).error)
            return res.status(400).send(debug_data);
        return res.status(200).send(debug_data);
    }
}

export default DebugController;