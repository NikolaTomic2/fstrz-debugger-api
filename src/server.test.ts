import server from './server';
import supertest from 'supertest';

describe('DebugController.debug_url()', () => {
    it('should reject a non url string', (done) => {
        supertest(server)
            .get('/?url=hello')
            .expect(406, done)
    });

    it('should reject a non url string', (done) => {
        supertest(server)
            .get('/?url=http://example.com')
            .expect(200, done)
    });

    after(() => {
        server.close();
    });
});